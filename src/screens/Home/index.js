import React from "react";
import Navbar from "../../components/Navbar";
import styles from "./Home.module.css";
import BoxGeneric from "../../components/BoxGeneric";
import Donate from "../../components/Donate";
import NeedDonate from "../../components/NeedDonate";
import Contact from "../../components/Contact";
import Support from "../../components/Support";
import axios from "axios";
import SeendEmail from "../../components/SendEmail";
import GraficData from "../../components/GraficData";

const Home = () => {
  const [selectedOption, setSelectedOption] = React.useState(0);
  const [error, setError] = React.useState(false);
  const [data, setData] = React.useState({});

  const handleFetchDashInformation = async () => {
    try {
     const response =  await axios
        .get("https://banco-de-sangue-virtual.onrender.com/doadoresDados")
      setData(response.data)
    } catch (err) {
      setError(true);
    }
  };

  React.useEffect(() => {
   handleFetchDashInformation()
  }, []);

  const handleSelectOption = (i) => {
    setSelectedOption(i);
  };
  return (
    <div className={styles.containerAllAplication}>
      <div className={styles.containerHomeAplication}>
        <Navbar handleSelectOption={handleSelectOption} />
        <div style={{ width: "100%" }}>
          <div className={styles.containerBoxGeneric}>
            {error ? (
              <p id="header">Erro ao carregar os cards</p>
            ) : (
              <>
                <BoxGeneric
                  title="Doadores Cadastrados"
                  value={data?.TotaldeDoadores}
                />
                <BoxGeneric
                  title="Doações Cadastradas"
                  value={data?.TotaldeDoacoes}
                />
                <BoxGeneric
                  title="Contatos Cadastrados"
                  value={data?.TotaldeContatos}
                />
              </>
            )}
          </div>
          <div className={styles.containerInformation}>
            {selectedOption === 0 && <GraficData />}
            {selectedOption === 1 && <Donate />}
            {selectedOption === 2 && <NeedDonate />}
            {selectedOption === 3 && <Contact />}
            {selectedOption === 4 && <Support />}
            {selectedOption === 5 && <SeendEmail />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
