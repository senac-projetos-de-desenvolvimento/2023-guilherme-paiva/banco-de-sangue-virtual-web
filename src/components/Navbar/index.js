import React from "react";
import BancoDeSangueLogo from "../../assets/logoHome.png";
import styles from "./Navbar.module.css";
import { navBarOption } from "../../utils/mocks";

const Navbar = ({ handleSelectOption }) => {
  return (
    <div className={styles.containerNavbar}>
      <div className={styles.containerImage}>
        <img
          src={BancoDeSangueLogo}
          alt="logo-banco-de-sangue"
          className={styles.image}
        />
      </div>
      <div className={styles.containerText}>
        {navBarOption.map((option, i) => (
          <p className={styles.text} onClick={() => handleSelectOption(i)}>
            {option}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Navbar;
