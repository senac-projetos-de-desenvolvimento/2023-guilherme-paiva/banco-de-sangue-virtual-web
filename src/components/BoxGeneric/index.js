import React from "react";
import styles from "./BoxGeneric.module.css";

const BoxGeneric = ({ value, title }) => {
  return (
    <div className={styles.boxGenericContainer}>
      <p className={styles.valueBoxGeneric}>{value}</p>
      <p className={styles.titleBoxGeneric}>{title}</p>
    </div>
  );
};

export default BoxGeneric;
