import React from "react";
import axios from "axios";
import { optionsSelectContactSupport } from "../../utils/mocks";
import Select from "../Select";

const Contact = () => {
  const [data, setData] = React.useState([]);
  const [filterResult, setFilterResult] = React.useState([]);
  const [selectOption, setSelectOption] = React.useState("");
  const [filterOption, setFilterOption] = React.useState("");
  const [error, setError] = React.useState(false);

  const handleFetchDashInformation = async () => {
    try {
      await axios
        .get("https://banco-de-sangue-virtual.onrender.com/contatos")
        .then((r) => setData(r.data));
    } catch {
      setError(true);
    }
  };

  React.useEffect(() => {
    handleFetchDashInformation();
  }, []);
  React.useEffect(() => {
    const resultFilter = data;
    if (selectOption === "nome") {
      const results = resultFilter.filter((resp) =>
        resp.nome.includes(filterOption)
      );
      setFilterResult(results);
    }

    if (selectOption === "email") {
      const results = resultFilter.filter((resp) =>
        resp.email.includes(filterOption)
      );
      setFilterResult(results);
    }
  }, [filterOption]);

  return (
    <div>
      {error ? (
        <p id="header">Erro ao carregar as informações</p>
      ) : (
        <>
          <p id="header">Contato</p>
          <Select
            setSelectOption={setSelectOption}
            options={optionsSelectContactSupport}
            filterOption={filterOption}
            selectOption={selectOption}
            setFilterOption={setFilterOption}
          />
          <table id="minhaTabela" border="1">
            <thead>
              <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Mensagem</th>
                <th>Data criação</th>
              </tr>
            </thead>
            <tbody>
              {data &&
                !filterOption &&
                data.map((user) => (
                  <tr>
                    <td>{user.nome}</td>
                    <td>{user.email}</td>
                    <td>{user.fone}</td>
                    <td>{user.mensagem}</td>
                    <td>
                      {new Date(user.createdAt).toLocaleDateString("pt-BR", {
                        year: "numeric",
                        month: "2-digit",
                        day: "2-digit",
                      })}
                    </td>
                  </tr>
                ))}
              {filterResult &&
                filterOption &&
                filterResult?.map((user) => (
                  <tr>
                    <td>{user.nome}</td>
                    <td>{user.email}</td>
                    <td>{user.fone}</td>
                    <td>{user.mensagem}</td>
                    <td>
                      {new Date(user.createdAt).toLocaleDateString("pt-BR", {
                        year: "numeric",
                        month: "2-digit",
                        day: "2-digit",
                      })}
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
};

export default Contact;
