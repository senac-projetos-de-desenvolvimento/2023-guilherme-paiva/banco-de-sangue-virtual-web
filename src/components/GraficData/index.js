import React from "react";
import { Bar } from "react-chartjs-2";
import "chart.js/auto";
import axios from "axios";

const GraficData = () => {
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [dados, setDados] = React.useState([]);

  const handleFetchDashInformation = async () => {
    try {
      await axios
        .get("https://banco-de-sangue-virtual.onrender.com/dadosDoadores")
        .then((r) => setDados(r.data));
    } catch (err) {
      setError(true);
    }
  };

  React.useEffect(() => {
    handleFetchDashInformation();
  }, []);

  const labels = dados.map((item) => item.tiposanguineo);
  const data = dados.map((item) => item.soma);

  const chartData = {
    labels: labels,
    datasets: [
      {
        label: "Total de Tipos Sanguineos Cadastrados",
        backgroundColor: "#ec5353",
        borderColor: "rgba(0,0,0,1)",
        borderWidth: 2,
        data: data,
      },
    ],
  };

  const chartOptions = {
    maintainAspectRatio: false,
    scales: {
      y: {
        beginAtZero: true,
        title: {
          display: true,
          text: "Quantidade",
        },
      },
      x: {
        type: "category",
        position: "bottom",
        title: {
          display: true,
          text: "Tipo Sanguineo",
        },
      },
    },
  };

  return (
    <div>
      {error ? (
        <p id="header">Erro ao carregar as informações</p>
      ) : (
        <div style={{ width: "100%", height: "30vh" }}>
          <p id="header">Gráfico Sanguíneos</p>
          <Bar data={chartData} options={chartOptions} />
        </div>
      )}
    </div>
  );
};

export default GraficData;
