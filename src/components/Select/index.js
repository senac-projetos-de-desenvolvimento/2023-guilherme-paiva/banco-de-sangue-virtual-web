import React from "react";

const Select = ({
  setSelectOption,
  options,
  selectOption,
  filterOption,
  setFilterOption,
  placeholderInput,
  defaultValue,
}) => {
  return (
    <div id="selectDiv">
      <select onChange={({ target }) => setSelectOption(target.value)}>
        <option value="" selected>
          {defaultValue ? defaultValue : "Filtrar por:"}
        </option>
        {options.map((op) => (
          <option value={op.value}>{op.nome}</option>
        ))}
      </select>
      {selectOption && (
        <input
          id="inputSelect"
          value={filterOption}
          placeholder={placeholderInput ? placeholderInput : "Filtrar por..."}
          onChange={({ target }) => setFilterOption(target.value)}
        />
      )}
    </div>
  );
};

export default Select;
