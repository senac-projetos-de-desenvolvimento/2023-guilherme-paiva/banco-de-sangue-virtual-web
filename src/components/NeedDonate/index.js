import React from "react";
import axios from "axios";
import Select from "../Select";
import { optionsSelectNeedDonate } from "../../utils/mocks";

const NeedDonate = () => {
  const [data, setData] = React.useState([]);
  const [filterResult, setFilterResult] = React.useState([]);
  const [selectOption, setSelectOption] = React.useState("");
  const [filterOption, setFilterOption] = React.useState("");
  const [error, setError] = React.useState(false);

  const handleFetchDashInformation = async () => {
    try {
      await axios
        .get("https://banco-de-sangue-virtual.onrender.com/doacoes")
        .then((r) => setData(r.data));
    } catch {
      setError(true);
    }
  };

  React.useEffect(() => {
    handleFetchDashInformation();
  }, []);
  React.useEffect(() => {
    const resultFilter = data;
    if (selectOption === "nome") {
      const results = resultFilter.filter((resp) =>
        resp.nome.includes(filterOption)
      );
      setFilterResult(results);
    }
    if (selectOption === "tiposanguineo") {
      const results = resultFilter.filter((resp) =>
        resp.tiposanguineo.includes(filterOption)
      );
      setFilterResult(results);
    }
    if (selectOption === "localdoacao") {
      const results = resultFilter.filter((resp) =>
        resp.localdoacao.includes(filterOption)
      );
      setFilterResult(results);
    }
  }, [filterOption]);
  return (
    <div>
      {error ? (
        <p id="header">Erro ao carregar as informações</p>
      ) : (
        <>
          <p id="header">Doações</p>
          <Select
            setSelectOption={setSelectOption}
            options={optionsSelectNeedDonate}
            filterOption={filterOption}
            selectOption={selectOption}
            setFilterOption={setFilterOption}
          />
          <table id="minhaTabela" border="1">
            <thead>
              <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Tipo Sanguineo</th>
                <th>Local de Doação</th>
                <th>Data criação</th>
              </tr>
            </thead>
            <tbody>
              {data &&
                !filterOption &&
                data?.map((user) => (
                  <tr>
                    <td>{user.nome}</td>
                    <td>{user.email}</td>
                    <td>{user.fone}</td>
                    <td>{user.tiposanguineo}</td>
                    <td>{user.localdoacao}</td>
                    <td>
                      {new Date(user.createdAt).toLocaleDateString("pt-BR", {
                        year: "numeric",
                        month: "2-digit",
                        day: "2-digit",
                      })}
                    </td>
                  </tr>
                ))}
              {filterResult &&
                filterOption &&
                filterResult?.map((user) => (
                  <tr>
                    <td>{user.nome}</td>
                    <td>{user.email}</td>
                    <td>{user.fone}</td>
                    <td>{user.tiposanguineo}</td>
                    <td>{user.localdoacao}</td>
                    <td>
                      {new Date(user.createdAt).toLocaleDateString("pt-BR", {
                        year: "numeric",
                        month: "2-digit",
                        day: "2-digit",
                      })}
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
};

export default NeedDonate;
