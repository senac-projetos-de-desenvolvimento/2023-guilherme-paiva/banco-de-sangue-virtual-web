import React from "react";
import axios from "axios";
import Select from "../Select";
import { optionsSelectDonate } from "../../utils/mocks";
import Trash from "../../assets/trashCan.png";

const Donate = () => {
  const [data, setData] = React.useState([]);
  const [filterResult, setFilterResult] = React.useState([]);
  const [selectOption, setSelectOption] = React.useState("");
  const [filterOption, setFilterOption] = React.useState("");
  const [error, setError] = React.useState(false);

  const handleFetchDashInformation = async () => {
    try {
      await axios
        .get("https://banco-de-sangue-virtual.onrender.com/doadores")
        .then((r) => setData(r.data))
    } catch (err) {
      setError(true);
    }
  };

  const handleDeleteDonate = async (nome, id) => {
    const result = window.confirm("Confirma a exclusão do doador:" + nome);

    if (result === true) {
      try {
        await axios
          .delete(`https://banco-de-sangue-virtual.onrender.com/doadores/${id}`)
          .then((r) => console.log(r.data))
          window.location.reload()
      } catch (err) {
        setError(true);
      }
    } else {
       return
    }
  };

  React.useEffect(() => {
      handleFetchDashInformation();
  }, []);
  

  React.useEffect(() => {
    const resultFilter = data;
    if (selectOption === "nome") {
      const results = resultFilter.filter((resp) =>
        resp.nome.includes(filterOption)
      );
      setFilterResult(results);
    }
    if (selectOption === "cidade") {
      const results = resultFilter.filter((resp) =>
        resp.cidade.includes(filterOption)
      );
      setFilterResult(results);
    }
    if (selectOption === "tiposanguineo") {
      const results = resultFilter.filter((resp) =>
        resp.tiposanguineo.includes(filterOption)
      );
      setFilterResult(results);
    }
  }, [filterOption]);

  return (
    <div>
      {error ? (
        <p id="header">Erro ao carregar as informações</p>
      ) : (
        <>
          <p id="header">Doadores</p>
          <Select
            setSelectOption={setSelectOption}
            options={optionsSelectDonate}
            filterOption={filterOption}
            selectOption={selectOption}
            setFilterOption={setFilterOption}
          />
          <div>
            <table id="minhaTabela" border="1">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>Telefone</th>
                  <th>Estado</th>
                  <th>Cidade</th>
                  <th>Sexo</th>
                  <th>Tipo Sanguineo</th>
                  <th>Onde nos conheceu</th>
                  <th>Data criação</th>
                  <th>Deletar</th>
                </tr>
              </thead>
              <tbody>
                {data &&
                  !filterOption &&
                  data?.map((user, i) => (
                    <tr>
                      <td>{user.nome}</td>
                      <td>{user.email}</td>
                      <td>{user.fone}</td>
                      <td>{user.estado}</td>
                      <td>{user.cidade}</td>
                      <td>{user.sexo}</td>
                      <td>{user.tiposanguineo}</td>
                      <td>{user.ondenosconheceu}</td>
                      <td>
                        {new Date(user.createdAt).toLocaleDateString("pt-BR", {
                          year: "numeric",
                          month: "2-digit",
                          day: "2-digit",
                        })}
                      </td>
                      <td onClick={() => handleDeleteDonate(user.nome, user.id)}>
                        <img src={Trash} alt="logo-banco-de-sangue-lixeira" />
                      </td>
                    </tr>
                  ))} 
                 {filterResult &&
                  filterOption &&
                  filterResult?.map((user) => (
                    <tr>
                      <td>{user.nome}</td>
                      <td>{user.email}</td>
                      <td>{user.fone}</td>
                      <td>{user.estado}</td>
                      <td>{user.cidade}</td>
                      <td>{user.sexo}</td>
                      <td>{user.tiposanguineo}</td>
                      <td>{user.ondenosconheceu}</td>
                      <td>
                        {new Date(user.createdAt).toLocaleDateString("pt-BR", {
                          year: "numeric",
                          month: "2-digit",
                          day: "2-digit",
                        })}
                      </td>
                      <td onClick={() => handleDeleteDonate(user.nome, user.id)}>
                        <img src={Trash} alt="logo-banco-de-sangue-lixeira" />
                      </td>
                    </tr>
                  ))}
                   
              </tbody>
            </table>
          </div>
        </>
      )}
    </div>
  );
};

export default Donate;
