import React from "react";
import axios from "axios";
import { tipoSanguineoMock } from "../../utils/mocks";
import Select from "../Select";

const SeendEmail = () => {
  const [selectOption, setSelectOption] = React.useState("");
  const [filterOption, setFilterOption] = React.useState("");
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleSendEmail = async () => {
    if (!filterOption && !selectOption) {
      alert("Insira todas as informações para enviar o e-mail");
      return;
    }
    setLoading(true);
    setError(false);
    try {
      await axios.get(
        `https://banco-de-sangue-virtual.onrender.com/email/${filterOption}/${selectOption}`
      );
      alert("E-mail enviado com sucesso");
    } catch (err) {
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <div style={{paddingBottom: '2rem'}} >
        <p id="header">Enviar E-mail Solicitando Doações</p>
  
        {!loading && (
          <>
            <Select
              setSelectOption={setSelectOption}
              options={tipoSanguineoMock}
              filterOption={filterOption}
              selectOption={selectOption}
              setFilterOption={setFilterOption}
              defaultValue="Selecione o tipo sanguineo"
              placeholderInput="Cidade"
            />
            <button onClick={handleSendEmail} style={{width: '30vw'}}>Enviar e-mail</button>
          </>
        )}
        {loading && <p id="header">Enviando e-mail aguarde um momento...</p>}
        {error && <p>Erro ao enviar e-mail tente novamente mais tarde</p>}
      </div>
      {/* <div>
        <p id="header">Envio de E-mail para Campanhas</p>
        {!loading && (
          <div
            style={{ display: "flex", flexDirection: "column", width: "100%" }}
          >
            <div style={{paddingBottom: '1rem'}}>
              <p id='textLabel'>Assunto: </p>
              <input id="inputSelect" style={{width: '30vw', marginLeft: '0px'}} />
            </div>
            <div style={{paddingBottom: '1rem'}} >
              <p id='textLabel'>Mensagem: </p>
              <textarea id="inputSelect" style={{width: '30vw', marginLeft: '0px'}} />
             
            </div>
           <button onClick={handleSendEmail} style={{width: '30vw'}}>Enviar e-mail</button>
          </div>
        )}
        {loading && <p id="header">Enviando e-mail aguarde um momento...</p>}
        {error && <p>Erro ao enviar e-mail tente novamente mais tarde</p>}
      </div> */}
      </div>
  );
};

export default SeendEmail;
