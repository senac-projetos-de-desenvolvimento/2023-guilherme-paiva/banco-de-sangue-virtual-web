

export const navBarOption = [
  "Gráfico",
  "Doadores",
  "Doações",
  "Contato",
  "Apoiadores",
  "Enviar E-mail",
];

export const estateMock = [
  "Acre",
  "Alagoas",
  "Amapá",
  "Amazonas",
  "Bahia",
  "Ceará",
  "Distrito Federal",
  "Espírito Santo",
  "Goiás",
  "Maranhão",
  "Mato Grosso",
  "Mato Grosso do Sul",
  "Minas Gerais",
  "Pará",
  "Paraíba",
  "Paraná",
  "Pernanbuco",
  "Piauí",
  "Rio de Janeiro",
  "Rio Grande do Norte",
  "Rio Grande do Sul",
  "Rondônia",
  "Roraima",
  "Santa Catarina",
  "São Paulo",
  "Sergipe",
  "Tocantins",
];

export const optionsSelectDonate = [
  {
    nome: "Nome",
    value: "nome",
  },
  {
    nome: "Cidade",
    value: "cidade",
  },
  {
    nome: "Tipo Sanguineo",
    value: "tiposanguineo",
  },
];

export const optionsSelectNeedDonate = [
  {
    nome: "Nome",
    value: "nome",
  },
  {
    nome: "Tipo Sanguineo",
    value: "tiposanguineo",
  },
  { nome: "Local de Doação", value: "localdoacao" },
];

export const optionsSelectContactSupport = [
  {
    nome: "Nome",
    value: "nome",
  },
  {
    nome: "E-mail",
    value: "email",
  },
];

export const tipoSanguineoMock = [
  {
    nome: "A+",
    value: "A+",
  },
  {
    nome: "A-",
    value: "A-",
  },
  {
    nome: "B+",
    value: "B+",
  },
  {
    nome: "B-",
    value: "B-",
  },
  {
    nome: "AB+",
    value: "AB+",
  },
  {
    nome: "AB-",
    value: "AB-",
  },
  {
    nome: "O+",
    value: "O+",
  },
  {
    nome: "O-",
    value: "O-",
  },
];
